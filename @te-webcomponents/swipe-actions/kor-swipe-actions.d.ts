import { LitElement } from 'lit';
/**
 * @slot - Contains the visible element to be scrolled left or right
 * @slot left - Contains the action shown when the user swipes to the left
 * @slot right - Contains the action shown when the user swipes to the right
 * @event swipe-right - Fired when the user swipes right more than 1/3 of the element's width
 * @event swipe-left - Fired when the user swipes left more than 1/3 of the element's width
 */
export declare class korSwipeActions extends LitElement {
    static get styles(): import("lit").CSSResultGroup[];
    render(): import("lit-html").TemplateResult<1>;
    connectedCallback(): void;
    setSlotWidth(tar: EventTarget | null): void;
    handleTouchEnd(): void;
    setSlotOpacity(): void;
    getSlotOpacity(name: string): number;
}
