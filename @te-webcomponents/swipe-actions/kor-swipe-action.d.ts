import { LitElement } from 'lit';
export declare class korSwipeAction extends LitElement {
    icon: string | undefined;
    bgColor: string | undefined;
    static get styles(): import("lit").CSSResultGroup[];
    render(): import("lit-html").TemplateResult<1>;
    attributeChangedCallback(name: string, oldval: string, newval: string): void;
    connectedCallback(): void;
}
