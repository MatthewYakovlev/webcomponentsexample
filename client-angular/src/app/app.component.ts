import { Component } from '@angular/core';
// import '@te-webcomponents/accordion'
// import '@te-webcomponents/app-bar'
import '@te-webcomponents'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'test-wc';

  isDisabledInput: boolean = false;

  alertClick(event: any): void {
    console.log('EVENT: ', event)
  }

  onChangeInput(event: any): void {
    const val = event.target.value
    console.log('Val: ', val) 
  }

  onKeyDown(event: Event): void {
    console.log('KEY DOWN: ', event)
  }

  onKeyUp(event: Event): void {
    console.log('KEY UP: ', event)
  }
}
